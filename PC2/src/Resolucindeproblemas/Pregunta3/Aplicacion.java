package Resolucindeproblemas.Pregunta3;
import java.util.ArrayList;
public class Aplicacion {
    public static void main(String[] args) {
        ArrayList<Pregunta> preguntas = new ArrayList<Pregunta>();
        preguntas.add(new Pregunta("1. ¿Qué establecimiento desea conocer de la UNI?","1. Museo.","2. Biblioteca central.","3. Coliseo"));
        preguntas.add(new Pregunta("2. ¿Qué facultad desea visitar?","1. FIIS","2. FIQM","3. FIA"));
        preguntas.add(new Pregunta("Para conoce los cambios de infraestructura física por favor responda. ¿Hace cuántos años no visita la UNI?","1. Un año.","2. Tres años.","3. Más de tres años."));
    }
}

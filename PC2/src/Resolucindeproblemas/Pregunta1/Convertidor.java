package Resolucindeproblemas.Pregunta1;
import java.util.Map;
import java.util.HashMap;
public class Convertidor {
    Map<String, Float> hashMap = new HashMap<String, Float>();
    public Convertidor()
    {
        hashMap.put("Dolares", 1f);
        hashMap.put("Yenes", 0.0092f);
        hashMap.put("Francos", 1.11f);
        hashMap.put("Euros", 1.22f);

    }
    public float cantidad;
    public  String clasemoneda;
   
    public float getCantidad() {
        return cantidad;
    }
    public String getClasemoneda() {
        return clasemoneda;
    }
    public void setClasemoneda(String clasemoneda) {
        this.clasemoneda = clasemoneda;
    }
    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }
    public float convertir(String clasemoneda, Float cantidad){
            if(hashMap.get(clasemoneda) == null)
            {
             return 0;
            }
            else
            {
                cantidad=cantidad*hashMap.get(clasemoneda);
                cantidad=cantidad*3.71f;
                return cantidad;
            }
    }
   
}

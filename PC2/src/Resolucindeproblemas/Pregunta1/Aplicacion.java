package Resolucindeproblemas.Pregunta1;
import java.util.*;
public class Aplicacion {
    public static void main(String[] args) {
        Scanner entrada= new Scanner(System.in);
        Convertidor moneditas= new Convertidor();
        System.out.println("Digite a que moneda quieres convertir: ");
        moneditas.clasemoneda=entrada.nextLine();
        System.out.println("Digite la cantidad quieres convertir: ");
        moneditas.cantidad=entrada.nextFloat();
        System.out.println(moneditas.convertir(moneditas.clasemoneda, moneditas.cantidad));
    }
}

package Problemadelahoja;

public class Destino {
    private String direccion;
    private String mediodetransporte;
    public Destino(String direccion, String mediodetransporte) {
        this.direccion = direccion;
        this.mediodetransporte = mediodetransporte;
    }
    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public String getMediodetransporte() {
        return mediodetransporte;
    }
    public void setMediodetransporte(String mediodetransporte) {
        this.mediodetransporte = mediodetransporte;
    }
    
}

package Problemadelahoja;

public class Pedido {
    private Cliente cliente;
    private double preciototal;
    private Fruta[] frutas= new Fruta[2];
    public Pedido(Cliente cliente, Fruta[] frutas) {
        this.cliente = cliente;
        this.frutas = frutas;
    }
    public void proceso(){
        double sumatoriaprecio=0;
        for(int i=0;i<2;i++){
            sumatoriaprecio+=frutas[i].multiplicado();
        }
        this.preciototal=sumatoriaprecio;
    }
    public void imprimir(){
    this.proceso();
    System.out.println("El cliente es: "+ cliente.getNombrecliente()+ " Y su destino es: "+ cliente.getDestino().getDireccion());
    System.out.println("El medio de transporte es: "+ cliente.getDestino().getMediodetransporte());
    System.out.println("El precio total es : "+ this.preciototal);
    }
        
    
    
}

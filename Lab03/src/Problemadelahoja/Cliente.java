package Problemadelahoja;

public class Cliente {
    private String nombrecliente;
    private Destino destino;
    public Cliente(String nombrecliente, Destino destino) {
        this.nombrecliente = nombrecliente;
        this.destino = destino;
    }
    public String getNombrecliente() {
        return nombrecliente;
    }
    public void setNombrecliente(String nombrecliente) {
        this.nombrecliente = nombrecliente;
    }
    public Destino getDestino() {
        return destino;
    }
    public void setDestino(Destino destino) {
        this.destino = destino;
    }
    
}

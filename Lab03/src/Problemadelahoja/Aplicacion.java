package Problemadelahoja;

public class Aplicacion {
    public static void main(String[] args) {

    Destino destino1= new Destino("Cuzco","Camion de carga");
    Cliente cliente1 = new Cliente("Pepe", destino1);
    Fruta[] frutas1= new Fruta[2];
    frutas1[0]= new Arandano(10);
    frutas1[1]= new Frambuesa(20);
    Pedido pedido1= new Pedido(cliente1, frutas1);
    pedido1.imprimir();
    System.out.println();
    Destino destino2= new Destino("Brasil","Transporte fluvial");
    Cliente cliente2 = new Cliente("Lucho", destino2);
    Fruta[] frutas2= new Fruta[2];
    frutas2[0]= new Maiz(10);
    frutas2[1]= new Alcachofa(20);
    Pedido pedido2= new Pedido(cliente2, frutas2);
    pedido2.imprimir();

    }
}

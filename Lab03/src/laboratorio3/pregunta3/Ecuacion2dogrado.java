package laboratorio3.pregunta3;

public class Ecuacion2dogrado extends Ecuacionabs {

    public void Hallarraices(){
        if(Validar()==true){
            double discriminante=Math.pow(coeficiente2, 2) - 4*coeficiente1*coeficiente3;
            raiz1= ((-coeficiente2)+Math.sqrt(discriminante))/(2*coeficiente1);
            raiz2= ((-coeficiente2)-Math.sqrt(discriminante))/(2*coeficiente1);
            System.out.println(this.raiz1);
            System.out.println(this.raiz2);
        }
        else{
            System.out.println("No hay raices reales.");
        }
    }
    public boolean Validar(){
        double discriminante=Math.pow(coeficiente2, 2) - 4*coeficiente1*coeficiente3;
        if(discriminante>=0){
            return true;
        }
        else{
            return false;
        }
    }
    
}

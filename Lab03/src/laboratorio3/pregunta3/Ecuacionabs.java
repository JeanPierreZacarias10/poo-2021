package laboratorio3.pregunta3;

public abstract class Ecuacionabs {
    protected double coeficiente1,coeficiente2,coeficiente3,raiz1,raiz2;
    public abstract void Hallarraices();
    public abstract boolean Validar();
    public double getCoeficiente1() {
        return coeficiente1;
    }

    public void setCoeficiente1(double coeficiente1) {
        this.coeficiente1 = coeficiente1;
    }

    public double getCoeficiente2() {
        return coeficiente2;
    }

    public void setCoeficiente2(double coeficiente2) {
        this.coeficiente2 = coeficiente2;
    }

    public double getCoeficiente3() {
        return coeficiente3;
    }

    public void setCoeficiente3(double coeficiente3) {
        this.coeficiente3 = coeficiente3;
    }

    public double getRaiz1() {
        return raiz1;
    }

    public void setRaiz1(double raiz1) {
        this.raiz1 = raiz1;
    }

    public double getRaiz2() {
        return raiz2;
    }

    public void setRaiz2(double raiz2) {
        this.raiz2 = raiz2;
    }
    
}

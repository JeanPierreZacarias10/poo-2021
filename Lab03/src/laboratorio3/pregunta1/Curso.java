package laboratorio3.pregunta1;

public class Curso {
    private String Código;
    private double Créditos; 
    private int horasTotales;
    public String getCódigo() {
        return Código;
    }
    public void setCódigo(String código) {
        Código = código;
    }
    public double getCréditos() {
        return Créditos;
    }
    public void setCréditos(double créditos) {
        Créditos = créditos;
    }
    public int getHorasTotales() {
        return horasTotales;
    }
    public void setHorasTotales(int horasTotales) {
        this.horasTotales = horasTotales;
    }
    
}

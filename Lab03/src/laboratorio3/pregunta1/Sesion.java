package laboratorio3.pregunta1;

public class Sesion {
    private Curso cursito;
    private Alumno alumnito[];
    public Sesion(Curso cursito, Alumno[] alumnito) {
        this.cursito = cursito;
        this.alumnito = alumnito;
    }
    public void Mostrar(){
        for (int i=0;i<alumnito.length;i++){
            System.out.println("El alumno "+(i+1)+ "es :");
            System.out.println(alumnito[i].getNombre());
            System.out.println(alumnito[i].getApellido());
            System.out.println(alumnito[i].getNota());
        }
    }
    
    /*Un docente quiere almacenar en una clase Sesion la información de sus estudiantes: 
    Nombres, Apellidos, Notas del curso.
    Adicionalmente requiere disponer de información del curso: Créditos, Código, horas totales de sesión.
    Utilice constructores para inicializar los objetos de la clase sesión.*/
}
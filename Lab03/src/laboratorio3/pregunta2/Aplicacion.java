package laboratorio3.pregunta2;

public class Aplicacion {
    public static void main(String[] args) {
        MotorFluvial motorcito = new MotorFluvial();
        MotorGenerico motorcitogenerico = new MotorGenerico();
        System.out.println("Este es el motor fluvial");
        motorcito.Encender();
        motorcito.Apagar();
        motorcito.Mostrar();
        motorcito.Manejar();
        System.out.println("Este es el motor generico");
        motorcitogenerico.Encender();
        motorcitogenerico.Apagar();
        motorcitogenerico.Mostrar();
    }
}

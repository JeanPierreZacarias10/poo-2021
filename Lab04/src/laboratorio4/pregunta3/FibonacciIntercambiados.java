package laboratorio4.pregunta3;

public class FibonacciIntercambiados {
    public static void SumarFibonacci(int cantidad){
        int numero1=0,numero2=1,sumadedos=0,sumafibonacci=0;
        for ( int i=0;i<cantidad;i++){
            if(i==0){
                sumafibonacci+=numero1;
            }
            else if(i==1){
                sumafibonacci+=(-numero2);
            }
            else if(i%2==0){
                sumadedos=numero1+numero2;
                numero1= numero2;
                numero2= sumadedos;
                sumafibonacci+=sumadedos;
            }
            else{
                sumadedos=numero1+numero2;
                numero1= numero2;
                numero2= sumadedos;
                sumafibonacci-=sumadedos;
            }
        }
        System.out.println(sumafibonacci);
    }
}

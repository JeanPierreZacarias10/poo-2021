package laboratorio4.Resoluciondelahoja.clasesabstracta;

public abstract class Carta {
    protected String nombre;
    protected Integer puntaje;
    public Carta(String nombre) {
        this.nombre = nombre;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public Integer getPuntaje() {
        return puntaje;
    }
    public void setPuntaje(Integer puntaje) {
        this.puntaje = puntaje;
    }
    
}

package laboratorio4.Resoluciondelahoja;

import laboratorio4.Resoluciondelahoja.clasesabstracta.*;
import laboratorio4.Resoluciondelahoja.clasesconcreta.*;

public class Aplicacion {
    public static void main(String[] args) {
        System.out.println("Jugador 1");
        Carta cartita1= new CartaR35(11);
        Carta cartita2= new CartaR35("K");
        Jugador jugador1= new Jugador(cartita1,cartita2);
        System.out.println("El puntaje es: "+ jugador1.sumatoria());
        System.out.println("Jugador 2");
        Carta cartita3= new CartaR36(10);
        Carta cartita4= new CartaR36("Q");
        Jugador jugador2= new Jugador(cartita3,cartita4);
        System.out.println("El puntaje es: "+ jugador2.sumatoria());
    }
}

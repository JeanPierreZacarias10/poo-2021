package laboratorio4.Resoluciondelahoja.clasesconcreta;

import laboratorio4.Resoluciondelahoja.clasesabstracta.Carta;

public class CartaR35 extends Carta {

    public CartaR35(String nombre) {
        super(nombre);
        if(nombre.equals("J")){
            this.puntaje=5;
        }
        else if(nombre.equals("Q")){
            this.puntaje=6;
        }
        else if(nombre.equals("K")){
            this.puntaje=8;
        }
        else if(nombre.equals("A")){
            this.puntaje=10;
        }
    }
    public CartaR35(Integer nombre) {
        super(nombre.toString());
        if(2<=nombre && nombre<=10){
            this.puntaje=4;
        }
        else if(nombre==11){
            this.puntaje=5;
        }
        else if(nombre==12){
            this.puntaje=6;
        }
        else if(nombre==13){
            this.puntaje=8;
        }
        else if(nombre==1){
            this.puntaje=10;
        }
    }
    
}

package laboratorio4.Resoluciondelahoja.clasesconcreta;

import laboratorio4.Resoluciondelahoja.clasesabstracta.Carta;

public class Jugador {
    private Carta[] cartas= new Carta[2];

    public Jugador(Carta carta1, Carta carta2) {
        this.cartas[0] = carta1;
        this.cartas[1] = carta2;
    }
    public int sumatoria(){
        return cartas[0].getPuntaje()+cartas[1].getPuntaje();
    }
}

package laboratorio4.Resoluciondelahoja.clasesconcreta;

import laboratorio4.Resoluciondelahoja.clasesabstracta.*;

public class CartaR36 extends Carta {

    public CartaR36(String nombre) {
        super(nombre);
        if(nombre.equals("J")){
            this.puntaje=6;
        }
        else if(nombre.equals("Q")){
            this.puntaje=4;
        }
        else if(nombre.equals("K")){
            this.puntaje=3;
        }
        else if(nombre.equals("A")){
            this.puntaje=2;
        }
    }
    public CartaR36(Integer nombre) {
        super(nombre.toString());
        if(2<=nombre && nombre<=10){
            this.puntaje=5;
        }
        else if(nombre==11){
            this.puntaje=6;
        }
        else if(nombre==12){
            this.puntaje=4;
        }
        else if(nombre==13){
            this.puntaje=3;
        }
        else if(nombre==1){
            this.puntaje=2;
        }
    }
    
}

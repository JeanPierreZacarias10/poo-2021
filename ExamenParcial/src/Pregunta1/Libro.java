package Pregunta1;
import java.util.*;
public class Libro {
    Map<String, String> mapadelibro;
    String titulo;
    String nombreautores;
    String codigo;
    
    String descripcion;
    String editorial;
    String añodepublicacion;
    String titulos;
    String resumen;
    public Libro(String titulo, String nombreautores, String codigo) {
        Map<String, String> mapadelibro= new HashMap<String, String>();
        mapadelibro.put("titulo", titulo);
        mapadelibro.put("autores", nombreautores);
        mapadelibro.put("codigo", codigo);
    }
    public Libro(String titulo, String nombreautores, String codigo, String descripcion, String editorial,
            String añodepublicacion, String titulos, String resumen) {
            this(titulo, nombreautores, codigo);
            mapadelibro.put("descripcion", descripcion);
            mapadelibro.put("editorial", editorial);
            mapadelibro.put("añodepublicacion", añodepublicacion);
            mapadelibro.put("resumen", resumen);
    }
    public boolean buscar(String criterio, String valor){
        return valor.equals(mapadelibro.get(criterio));
    }
}
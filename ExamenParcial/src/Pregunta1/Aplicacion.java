package Pregunta1;
import java.util.*;
public class Aplicacion {
    static List<Libro> libro;
    public static void main(String[] args) {
        String criterio,valor;
        Scanner entrada= new Scanner(System.in);
        inicializa();
        System.out.println("Cual es el criterio: ");
        criterio=entrada.nextLine();
        System.out.println("Cual es el valor: ");
        valor=entrada.nextLine();
        for (Libro element : libro) {
           if(element.buscar(criterio, valor)){
            for(Map.Entry<String, String> registro : element.mapadelibro.entrySet())
		{
			System.out.println(registro.getKey() + ": " + registro.getValue());
		}
           }

        }
        
        
    }
    public static void inicializa(){
        List<Libro> libro= new ArrayList<Libro>();
        libro.add(new Libro("Cronicas de una muerte anunciada", "Gabriel garcia Marquez","202020"));
        libro.add(new Libro("Los Miserables", "Victor Hugo","202021"));
        libro.add(new Libro("Paco yunque", "Cesar Vallejo","202022"));
        libro.add(new Libro("Trilce", "Cesar Vallejo","202023"));
    }
    
}

package Pregunta2;
import java.lang.*;
public class Postulante implements Comparable {
    
    String DNI;
    String nombre;
    String apellidos;
    public double getNota() {
        return nota;
    }
    public void setNota(double nota) {
        this.nota = nota;
    }
    String niveleducativo;
    String dirección;
    Long número;
    int edad;
    double nota=0;
    public Postulante(String dNI, String nombre, String apellidos, String niveleducativo, String dirección, Long número,
            int edad) {
        DNI = dNI;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.niveleducativo = niveleducativo;
        this.dirección = dirección;
        this.número = número;
        this.edad = edad;
        this.nota=0;
    }
    @Override
    public int compareTo(Object o) {
        
        return this.DNI.compareTo(((Postulante)o).DNI);
    }

}

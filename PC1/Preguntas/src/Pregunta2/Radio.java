package Pregunta2;

public class Radio {
    private double radio=3;
    private double diagonales;
    private double perimetro;
    public double getDiagonales() {
        return diagonales;
    }
    public void setDiagonales(double diagonales) {
        this.diagonales = diagonales;
    }
    public double getPerimetro() {
        return perimetro;
    }
    public void setPerimetro(double perimetro) {
        this.perimetro = perimetro;
    }
    public double getRadio() {
        return radio;
    }
    public void setRadio(double radio) {
        this.radio = radio;
    }
    


}

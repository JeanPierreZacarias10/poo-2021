package Pregunta2;

public class Calcular {
    public static void totaldiagonales(Radio calculos){
        System.out.println("El total de diagonales de "+ calculos.getRadio() + " lados es "+ (calculos.getRadio()*(calculos.getRadio()-3))/2);
    }
    public static void area(Radio calculos, double valor){
        if( valor==3d){
            System.out.println("El area de "+ valor + " lados es "+ Math.sqrt(valor)*calculos.getRadio()*calculos.getRadio()/4); 
        }
        else if( valor==4d){
            System.out.println("El area de "+ valor + " lados es "+ calculos.getRadio()*calculos.getRadio()); 
        }
        else if( valor==5d){
            System.out.println("El area de "+ valor + " lados es "+ 5*calculos.getRadio()*calculos.getRadio()/(2*1.45)); 
        }
        else if( valor==6d){
            System.out.println("El area de "+ valor + " lados es "+ 6*(calculos.getRadio()*calculos.getRadio()*1.73)/4); 
        }
        
    }
}

package resolucionsemana2;
import java.util.Scanner;
public class resolucionconvesioncelsiuskelvin {
    public static void main(String[] args){
        Scanner entrada = new Scanner (System.in);
        int numero1,base1,modificado1=0,invertido1=0,longitud1;
        int suma=0,finalizado;
        System.out.println("Ingrese el grado en celsius: ");
        numero1 = entrada.nextInt();
        base1 = 8;
        if((validarbase(numero1,base1)== true)){
            invertido1 = invertir(numero1);
            longitud1=longitud(numero1);       
            modificado1=cambiardebasea10(invertido1,base1,longitud1);
            suma=modificado1+273;
            finalizado=cambiardebasea8(suma);
            System.out.println("El resultado en Kelvin en base 8 es "+finalizado);
            }
        else{
            System.out.println("El numero es incorrecto");
        }
    }
    public static int invertir(int numero){
        int invertido=0,resto;
        while(numero>0){
            resto=numero%10;
            invertido=invertido*10+resto;
            numero/=10;
        }
        return invertido;
    }
    public static boolean  validarbase(int numero, int base){
        int probado=0;
        while(numero > 0) {
            probado= numero % 10;
            numero = numero / 10;
            if(probado>=base){
                return false;
            }
        }
        return true;
    }
    public static int  longitud(int numero){
        int cifras=0;
        while(numero!=0){
            numero = numero/10;        
            cifras++; 
        }            
        return cifras;         
    }    
    public static int cambiardebasea10(int numero, int base, int longitud){
        int resultadofinal=0,residuo,elevador;
        for(int i=longitud-1;i>=0;i--){
            residuo=numero%10;
            elevador = (int)  Math.pow(base,i);
            resultadofinal=resultadofinal+ residuo*elevador;
            numero/=10;
        }
        return resultadofinal;
    }
    public static int cambiardebasea8(int suma){
        int contador=0,numero_final=0,cociente=0,resta=0,basico=8,modificar=0,potenciacion;
        modificar=suma;
        while(modificar>0){
            resta = modificar % basico;
            cociente =  modificar/basico;
            modificar = cociente;
            potenciacion=(int)  Math.pow(10,contador);
            numero_final = (resta*potenciacion) + numero_final;
            contador = contador + 1;
        }
        potenciacion=(int)  Math.pow(10,contador);
        numero_final = cociente* potenciacion + numero_final;
        return numero_final;
    }
}
